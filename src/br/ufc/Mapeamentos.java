package br.ufc;

import java.util.HashMap;
import java.util.Map;

public class Mapeamentos {

	private int type;
	private Map<String,Integer> mapeamentoHours;
	private Map<String,Integer> mapeamentoHashTags;
	private Map<String,Integer> mapeamentoWords;
	private int contHoursError = 0;
	private int contWordsError = 0;
	private int contHashTagsError = 0;
	private int contTwitterError= 0;
    
	public Mapeamentos(Map<String, Integer> mapeamentoHours,
			Map<String, Integer> mapeamentoWords,
			Map<String, Integer> mapeamentoHashTags,
			int contHoursError,
			int contWordsError, int contHashTagsError, int contTwitterError) {
		super();
		this.mapeamentoHours = mapeamentoHours;
		this.mapeamentoHashTags = mapeamentoHashTags;
		this.mapeamentoWords = mapeamentoWords;
		this.contHoursError = contHoursError;
		this.contWordsError = contWordsError;
		this.contHashTagsError = contHashTagsError;
		this.contTwitterError = contTwitterError;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Map<String, Integer> getMapeamentoHours() {
		return mapeamentoHours;
	}
	public void setMapeamentoHours(Map<String, Integer> mapeamentoHours) {
		this.mapeamentoHours = mapeamentoHours;
	}
	public Map<String, Integer> getMapeamentoHashTags() {
		return mapeamentoHashTags;
	}
	public void setMapeamentoHashTags(Map<String, Integer> mapeamentoHashTags) {
		this.mapeamentoHashTags = mapeamentoHashTags;
	}
	public Map<String, Integer> getMapeamentoWords() {
		return mapeamentoWords;
	}
	public void setMapeamentoWords(Map<String, Integer> mapeamentoWords) {
		this.mapeamentoWords = mapeamentoWords;
	}
	public int getContHoursError() {
		return contHoursError;
	}
	public void setContHoursError(int contHoursError) {
		this.contHoursError = contHoursError;
	}
	public int getContWordsError() {
		return contWordsError;
	}
	public void setContWordsError(int contWordsError) {
		this.contWordsError = contWordsError;
	}
	public int getContHashTagsError() {
		return contHashTagsError;
	}
	public void setContHashTagsError(int contHashTagsError) {
		this.contHashTagsError = contHashTagsError;
	}
	public int getContTwitterError() {
		return contTwitterError;
	}
	public void setContTwitterError(int contTwitterError) {
		this.contTwitterError = contTwitterError;
	}
	
	
    
    
}
