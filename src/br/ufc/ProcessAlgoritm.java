package br.ufc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ProcessAlgoritm {
	public static boolean ASC = true;
    public static boolean DESC = false;
    
    public static final int WARRIORS = 0;
    public static final int CAVS = 1;
    public static final float ERROR = 0;
    public static final int LIMIT_TOTAL = 49889;
    public static final int LIMIT_TEST = 30000;
    public static final int LIMIT_LEARNING = 10000;
    
    static Mapeamentos mapWarriors;
    static Mapeamentos mapCavs;

  public static void main(String[] args) {

	ProcessAlgoritm obj = new ProcessAlgoritm();
	
	List<Twitter> twittersWarriors =  getTwitters("warriors.csv");
	List<Twitter> twittersCavs =  getTwitters("cavs.csv");

	List<Twitter> twittersWarriorsForTest = twittersWarriors.subList(LIMIT_TEST, LIMIT_TOTAL);
	List<Twitter> twittersCavsForTest = twittersCavs.subList(LIMIT_TEST, LIMIT_TOTAL);
	System.out.println("Size: " + twittersWarriorsForTest.size());
	System.out.println("Size: " + twittersCavsForTest.size());


	mapWarriors = obj.run(twittersWarriors.subList(0, LIMIT_TEST));
	mapCavs= obj.run(twittersCavs.subList(0, LIMIT_TEST));
	//List<Twitter> twitters = getTwitters("twitter.min.csv");
	
	int totalRIGHT = 0;
	int totalWRONG = 0;
	int totalUNDEFINED = 0;
	for(int i=0;i<LIMIT_LEARNING;i++){
		Twitter twitter = twittersWarriorsForTest.get(i);	
		double f1 = 0;
		double f2 = 0;
		
		f1 = getHoursWeight(twitter, WARRIORS) + getWordsWeight(twitter, WARRIORS) + getHashTagsWeight(twitter, WARRIORS);
		f2 = getHoursWeight(twitter, CAVS) + getWordsWeight(twitter, CAVS) + getHashTagsWeight(twitter, CAVS);
		
		if((f1 + ERROR) > f2){
			//System.out.println("WARRIORS");
			totalRIGHT++;
		}else{
			if((f2 + ERROR) > f1){
				//System.out.println("CAVS");
				totalWRONG++;
			}else{
				//System.out.println("INDEFINIDO");
				totalUNDEFINED++;
			}
		}
	}
	
	int totalRIGHT2 = 0;
	int totalWRONG2 = 0;
	int totalUNDEFINED2 = 0;
	for(int i=0;i<LIMIT_LEARNING;i++){
		Twitter twitter = twittersCavsForTest.get(i);	
		double f1 = 0;
		double f2 = 0;
		
		f1 = getHoursWeight(twitter, WARRIORS) + getWordsWeight(twitter, WARRIORS) + getHashTagsWeight(twitter, WARRIORS);
		f2 = getHoursWeight(twitter, CAVS) + getWordsWeight(twitter, CAVS) + getHashTagsWeight(twitter, CAVS);
		
		if((f1 + ERROR) > f2){
			//System.out.println("WARRIORS");
			totalWRONG2++;
		}else{
			if((f2 + ERROR) > f1){
				//System.out.println("CAVS");
				totalRIGHT2++;
			}else{
				//System.out.println("INDEFINIDO");
				totalUNDEFINED2++;
			}
		}
	}
	System.out.println("----TOTAL WARRIORS -----");
	System.out.println("Acertos: " +totalRIGHT);
	System.out.println("Erros: " + totalWRONG);
	System.out.println("N�o identificados: " + totalUNDEFINED);
	
	System.out.println("\n----TOTAL CAVS -----");
	System.out.println("Acertos: " +totalRIGHT2);
	System.out.println("Erros: " + totalWRONG2);
	System.out.println("N�o identificados: " + totalUNDEFINED2);
	
	//System.out.println("Pesos hora WARRIORS: " + getHoursWeight(twitter, WARRIORS));
		//System.out.println("Pesos hora CAVS: " + getHoursWeight(twitter, CAVS));

		//System.out.println("Pesos palavras WARRIORS: " + getWordsWeight(twitter, WARRIORS));
		//System.out.println("Pesos palavras CAVS: " + getWordsWeight(twitter, CAVS));
		
		//System.out.println("Pesos hashtags WARRIORS: " + getHashTagsWeight(twitter, WARRIORS));
		//System.out.println("Pesos hashtags CAVS: " + getHashTagsWeight(twitter, CAVS));
  }
  
  public static double getHoursWeight(Twitter twitter, int selected){
	  String key = getHoursByText(twitter.getTime());
	  int qntWarriors = 0;
	  int qntCavs = 0;
	  if(mapWarriors.getMapeamentoHours().containsKey(key)){
		  qntWarriors = mapWarriors.getMapeamentoHours().get(key);
	  }
	  if(mapCavs.getMapeamentoHours().containsKey(key)){
		  qntCavs = mapCavs.getMapeamentoHours().get(key);
	  }
	  if(WARRIORS==selected){ 
		  return calcWeight(qntCavs+qntWarriors,qntWarriors);
	  }else{
		  return calcWeight(qntCavs+qntWarriors,qntCavs);
	  }
  }
  
  public static double getWordsWeight(Twitter twitter, int selected){
	  String[] words = twitter.getText().split(" ");
	  int sumWarriors = 0;
	  int sumCavs = 0;
	  for(String key:words){	
		  int qntWarriors = 0;
		  int qntCavs = 0;
		  if(mapWarriors.getMapeamentoWords().containsKey(key)){
				qntWarriors = mapWarriors.getMapeamentoWords().get(key);
		  }
		  if(mapCavs.getMapeamentoWords().containsKey(key)){
				qntCavs = mapCavs.getMapeamentoWords().get(key);
		  }
		  sumWarriors += calcWeight(qntWarriors + qntCavs , qntWarriors);
		  sumCavs += calcWeight(qntWarriors + qntCavs , qntCavs);
		  //System.out.println("Word: " + key + " - qntWarriors: " + calcWeight(qntWarriors + qntCavs , qntWarriors) + " - qntCavs: " + calcWeight(qntWarriors + qntCavs , qntCavs));
	  }
	  if(WARRIORS==selected){ 
		  return sumWarriors/words.length;
	  }else{
		  return sumCavs/words.length;
	  }
  }
  
  public static double getHashTagsWeight(Twitter twitter, int selected){
	  String[] hashtags = twitter.getText().split(" ");
	  int sumWarriors = 0;
	  int sumCavs = 0;
	  int length = 0;
	  for(String key:hashtags){
		  if(key.contains("#")){
			  length++;
			  int qntWarriors = 0;
			  int qntCavs = 0;
			  if(mapWarriors.getMapeamentoHashTags().containsKey(key)){
					qntWarriors = mapWarriors.getMapeamentoHashTags().get(key);
			  }
			  if(mapCavs.getMapeamentoHashTags().containsKey(key)){
					qntCavs = mapCavs.getMapeamentoHashTags().get(key);
			  }
			  sumWarriors += calcWeight(qntWarriors + qntCavs , qntWarriors);
			  sumCavs += calcWeight(qntWarriors + qntCavs , qntCavs);
			  //System.out.println("Hashtag: " + key + " - qntWarriors: " + qntWarriors + " - qntCavs: " + qntCavs);
		  }
	  }
	  if(length==0) return 0;
	  if(WARRIORS==selected){ 
		  return sumWarriors/length;
	  }else{
		  return sumCavs/length;
	  }
  }
  
  public static List<Twitter> getTwitters(String csvFile){
	  BufferedReader br = null;
	  String line = "";
	  String cvsSplitBy = ",";
	  List<Twitter> twitters = new ArrayList<Twitter>();
	  try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] twitter = line.split(cvsSplitBy);
				try {
					twitters.add(new Twitter(twitter));
				}catch(ArrayIndexOutOfBoundsException e){
				}
			}
	  } catch (FileNotFoundException e) {
			e.printStackTrace();
	  } catch (IOException e) {
			e.printStackTrace();
	  } finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	  }	
	  return twitters;
  }

  public Mapeamentos run(List<Twitter> twitters ) {
	//System.out.println("Size: " + twitters.size());
	String lineSplitBySpace = " ";
	System.out.println("");

    Map<String,Integer> mapeamentoHours = new HashMap<String,Integer>();
    Map<String,Integer> mapeamentoHashTags = new HashMap<String,Integer>();
    Map<String,Integer> mapeamentoWords = new HashMap<String,Integer>();
	int contHoursError = 0;
	int contWordsError = 0;
	int contHashTagsError = 0;
	int contTwitterError= 0;
	
  	for(Twitter twitter:twitters){
			//get All hours
			try{		
				String key = getHoursByText(twitter.getTime());
				if(!key.contains("#")){
					if(mapeamentoHours.containsKey(key)){
						int value = mapeamentoHours.get(key);
						mapeamentoHours.put(key,value + 1);
					}else{
						mapeamentoHours.put(key,0);
					}
				}			
			}catch(ArrayIndexOutOfBoundsException e){
				//System.out.println("Error " + cont);
				contHoursError++;
			}catch (StringIndexOutOfBoundsException e) {
				contHoursError++;
			}
			
			//System.out.println("STEP 2 - GET ALL Words");
			//get All words
			try{		
				String[] words = twitter.getText().split(lineSplitBySpace);
				//System.out.println("[chave= " + words[1]  + "]");
				for(String key:words){	
					if(mapeamentoWords.containsKey(key)){
						int value = mapeamentoWords.get(key);
						mapeamentoWords.put(key,value + 1);
					}else{
						mapeamentoWords.put(key,0);
					}
				}
			}catch(ArrayIndexOutOfBoundsException e){
				contWordsError++;
			}
			
			//System.out.println("STEP 3 - GET ALL HashTags");
			//get All HashTags
			try{		
				String[] words = twitter.getText().split(lineSplitBySpace);
				//System.out.println("[chave= " + words[1]  + "]");
				for(String key:words){	
					if(key.contains("#") && !key.contains("##")){
						if(mapeamentoHashTags.containsKey(key)){
							int value = mapeamentoHashTags.get(key);
							mapeamentoHashTags.put(key,value + 1);
						}else{
							mapeamentoHashTags.put(key,0);
						}
					}
					
				}
			}catch(ArrayIndexOutOfBoundsException e){
				contHashTagsError++;
			}
  		}
		Map<String, Integer> sortedMapAsc;
		
        sortedMapAsc = sortByComparator(mapeamentoHours, DESC);
        //printMap(sortedMapAsc, 24);

        sortedMapAsc = sortByComparator(mapeamentoWords, DESC);
        //printMap(sortedMapAsc, 10);

        sortedMapAsc = sortByComparator(mapeamentoHashTags, DESC);
        //printMap(sortedMapAsc, 10);
		
		//System.out.println("----Erros---");
		//System.out.println("Hours:" + contHoursError);
		//System.out.println("Words:" + contWordsError);
		//System.out.println("HastTags:" + contHashTagsError);
		//System.out.println("Twitter:" + contTwitterError);

	System.out.println("------Done------");
	return new Mapeamentos(mapeamentoHours,mapeamentoWords,mapeamentoHashTags,
			contHoursError,contWordsError, contHashTagsError, contTwitterError);
  }
  
  private Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order)
  {

      List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());
      // Sorting the list based on values
      Collections.sort(list, new Comparator<Entry<String, Integer>>(){
          public int compare(Entry<String, Integer> o1,Entry<String, Integer> o2){
              if (order){
                  return o1.getValue().compareTo(o2.getValue());
              }else{
            	  return o2.getValue().compareTo(o1.getValue());}
          }
      });
      // Maintaining insertion order with the help of LinkedList
      Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
      for (Entry<String, Integer> entry : list){
          sortedMap.put(entry.getKey(), entry.getValue());
      }

      return sortedMap;
  }
  
  public static void printMap(Map<String, Integer> map)
  {
      for (Entry<String, Integer> entry : map.entrySet())
      {
    		  System.out.println(entry.getKey() + " - " + entry.getValue());
      }
  }
  
  public static void printMap(Map<String, Integer> map, int value)
  {
	  int interator = 0;
      for (Entry<String, Integer> entry : map.entrySet())
      {  
    	  if(interator==value){
    		  return;
    	  }
    	  System.out.println(entry.getKey() + " - " + entry.getValue());
    	  interator++;
      }
  }
  
  public static String getHoursByText(String text){
	  if(text.length()<13){
		  return "##";
	  }
	  return text.substring(11, 13);
  }
  public static double calcWeight(int total, int part){
	 if(total==0){
		 return 0;
	 }
	 return ((100*part)/total);   
  }
  

}
