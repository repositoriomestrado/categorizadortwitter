package br.ufc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadCVS {

  public static void main(String[] args) {

	ReadCVS obj = new ReadCVS();
	obj.run();

  }

  public void run() {
	// LivermoreTrends,
	// RT @jrnba: The @warriors and @festus hosted an awesome summer camp at Livermore!,
	// 14:07,
	// 28/06/2016 14:07:39,
	// en,
	// 2810251519,
	// "Livermore, CA"

	String csvFile = "twitter.csv";
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ",";
	String lineSplitBy = ":";
	List<Twitter> twitters = new ArrayList<Twitter>();
	
    Map<String,Integer> mapeamento = new HashMap<String,Integer>();
    for(int i=0;i<10;i++){
    	mapeamento.put("0" + i, 0);
    }
    for(int i=10;i<25;i++){
    	mapeamento.put(i + "", 0);
    }
    mapeamento.put("r!", 0);
	try {
		int cont = 0;
		br = new BufferedReader(new FileReader(csvFile));
		while ((line = br.readLine()) != null) {
			String[] twitter = line.split(cvsSplitBy);
			//from_user, text, hora, time, user_lang, from_user_id_str, user_location
			try{			
				String key = twitter[3].split(lineSplitBy)[0];
				System.out.println("[size= " + key + "]");
				if(key.length()==13){
					key = key.substring(11,13);
					System.out.println("[hora= " + key  + "]");
					int value = mapeamento.get(key);
					mapeamento.put(key,value + 1);
					twitters.add(new Twitter(twitter));
				}
			}catch(ArrayIndexOutOfBoundsException e){
				System.out.println("Error " + cont);
				cont++;
			}
		}
		for(int i=0;i<10;i++){
			int soma = mapeamento.get("0" + i);
			System.out.println(soma);
	    }
	    for(int i=10;i<25;i++){
	    	int soma = mapeamento.get(i + "");
			//System.out.println("[hora= " + i +" - "+ soma  + "]");
			System.out.println(soma);

	    }

	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	System.out.println("Done");
  }
  /*try {
		int cont = 0;
		br = new BufferedReader(new FileReader(csvFile));
		while ((line = br.readLine()) != null) {
			String[] twitter = line.split(cvsSplitBy);
			//from_user, text, hora, time, user_lang, from_user_id_str, user_location
			try{			
				String[] words = twitter[2].split(lineSplitBySpace);
				//System.out.println("[chave= " + words[1]  + "]");
				for(String key:words){	
					if(mapeamento.containsKey(key)){
						int value = mapeamento.get(key);
						mapeamento.put(key,value + 1);
					}else{
						mapeamento.put(key,0);
					}
				}
				twitters.add(new Twitter(twitter));

			}catch(ArrayIndexOutOfBoundsException e){
				System.out.println("Error " + cont);
				cont++;
			}
		}
		int interator = 0;
      Map<String, Integer> sortedMapAsc = sortByComparator(mapeamento, ASC);
      printMap(sortedMapAsc);
		
		System.out.println("Erros: " + cont);

	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}*/
}
