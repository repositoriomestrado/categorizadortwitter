package br.ufc;

public class Twitter {
	
	private String from_user, text, hora, time, user_lang, from_user_id_str, user_location;

	public Twitter(String from_user, String text, String hora, String time, String user_lang, String from_user_id_str,
			String user_location) {
		super();
		this.from_user = from_user;
		this.text = text;
		this.hora = hora;
		this.time = time;
		this.user_lang = user_lang;
		this.from_user_id_str = from_user_id_str;
		this.user_location = user_location;
	}
	public Twitter(String[] list) {
		super();
		this.from_user = list[0];
		this.text = list[1];
		this.hora = list[2];
		this.time = list[3];
		this.user_lang = list[4];
		this.from_user_id_str = list[5];
		this.user_location = list[6];
	}

	public String getFrom_user() {
		return from_user;
	}

	public void setFrom_user(String from_user) {
		this.from_user = from_user;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getUser_lang() {
		return user_lang;
	}

	public void setUser_lang(String user_lang) {
		this.user_lang = user_lang;
	}

	public String getFrom_user_id_str() {
		return from_user_id_str;
	}

	public void setFrom_user_id_str(String from_user_id_str) {
		this.from_user_id_str = from_user_id_str;
	}

	public String getUser_location() {
		return user_location;
	}

	public void setUser_location(String user_location) {
		this.user_location = user_location;
	}
	
	

}
